import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import logo from '../../resources/img/THE_3D_JEWEL_NAME.png'

const NavBar = () => {
    return (
        <AppBar position="static">
            <Toolbar style={styles.toolbar}>
                <img src={logo} width={250} height={50}/>
            </Toolbar>
        </AppBar>
    );
}

const styles = ({
    toolbar: {
        backgroundColor: '#000'
    }
})

export default NavBar