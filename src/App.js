import './App.css';
import {Component} from "react";
import NavBar from "./shared/NavBar/NavBar";
import Home from "./components/Home/Home";
import {BrowserRouter as Router} from 'react-router-dom';
class App extends Component{
  render() {
    return(
        <Router>
            <NavBar/>
            <Home/>
        </Router>
    )
  }
}
export default App;
